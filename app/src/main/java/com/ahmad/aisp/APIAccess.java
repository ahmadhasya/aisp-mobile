package com.ahmad.aisp;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class APIAccess {
    public JSONObject POST(String tujuan,String Bearer,String JsonInput){
        URL url;
        HttpURLConnection client = null;
        try {
            url = new URL("http://192.168.43.160:8000/"+tujuan);
            client = (HttpURLConnection) url.openConnection();
            client.setRequestMethod("POST");

            client.setDoInput (true);
            client.setDoOutput (true);
            client.setUseCaches (false);
            client.setRequestProperty("Content-Type","application/json");
            client.setRequestProperty("Accept","application/json");
            if(Bearer != "no"){
                client.setRequestProperty("Authorization","Bearer "+Bearer);
            }
            client.connect();
            //Create JSONObject here
            JSONObject jsonParam = new JSONObject(JsonInput);

            OutputStreamWriter out = new   OutputStreamWriter(client.getOutputStream());
            out.write(jsonParam.toString());
            out.close();
            int responseCode = client.getResponseCode();
            InputStream inputStream ;
            if (responseCode != HttpURLConnection.HTTP_OK) {
                inputStream = client.getErrorStream();
            }else{
                inputStream = client.getInputStream();
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line+"\n");
            }
            br.close();
            JSONObject ObjectResponse = new JSONObject(sb.toString());
            return ObjectResponse;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if(client != null){
                client.disconnect();
            }
        }
        return null;
    }

    public JSONObject GET(String tujuan,String Bearer){
        URL url;
        HttpURLConnection client = null;
        try {
            String addr = "http://192.168.43.160:8000/"+tujuan;
            url = new URL(addr);
            client = (HttpURLConnection) url.openConnection();
            client.setUseCaches(false);
            client.setAllowUserInteraction(false);
            if(Bearer != "no"){
                client.setRequestProperty("Authorization","Bearer "+Bearer);
            }
            InputStream inputStream ;
            if (client.getResponseCode() != HttpURLConnection.HTTP_OK) {
                inputStream = client.getErrorStream();
            }else{
                inputStream = client.getInputStream();
            }
            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
            StringBuilder sb = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line+"\n");
            }
            br.close();
            String JsonString = sb.toString();
            JSONObject ObjectResponse = new JSONObject(JsonString);
            return ObjectResponse;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if(client != null){
                client.disconnect();
            }
        }
        return null;
    }
}
