package com.ahmad.aisp;

import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class AddPeminjamanActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, TextWatcher, View.OnClickListener {

    AppCompatSpinner addbarang;
    TextInputEditText ETJumlah,ETSisa,ETJumlahPinjam;
    Button Tambah;
    TextView ngetes;

    ArrayList<String> kodes;
    ArrayList<Integer> jumlahs,ids;

    String access_token,kode;
    int Sisa,Jumlah,JumlahPinjam,id_inventaris;

    SimpleAdapter simpleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_peminjaman);
        addbarang = (AppCompatSpinner) findViewById(R.id.SPINBarang);
        ETJumlah = (TextInputEditText)findViewById(R.id.jumlah);
        ETSisa = (TextInputEditText)findViewById(R.id.jumlahSisa);
        ETJumlahPinjam = (TextInputEditText)findViewById(R.id.jumlahPinjam);
        Tambah = (Button)findViewById(R.id.tambahPeminjaman);

        ids   = new ArrayList<Integer>();
        kodes = new ArrayList<String>();
        jumlahs = new ArrayList<Integer>();

        Bundle data = getIntent().getExtras();

        access_token = data.getString("access_token");
        if(data.getStringArrayList("kodes")!=null){
            ids = data.getIntegerArrayList("ids");
            kodes = data.getStringArrayList("kodes");
            jumlahs = data.getIntegerArrayList("jumlahs");
        }

        listBarang();
        addbarang.setOnItemSelectedListener(this);
        ETJumlahPinjam.addTextChangedListener(this);
        Tambah.setOnClickListener(this);
    }

    void listBarang(){
        APIAccess API = new APIAccess();
        JSONObject response = API.GET("api/allPeminjaman",access_token);
        JSONArray data = null;
        try {
            data = response.getJSONArray("data");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayList<HashMap<String,String>> arrayList=new ArrayList<>();
        for(int i=0; i < data.length() ; i++) {
            JSONObject json_data = null;
            int id=0;
            String name = null,kode = null;
            try {
                json_data = data.getJSONObject(i);
                id=json_data.getInt("id_inventaris");
                name= json_data.getString("nama");
                kode= json_data.getString("kode_inventaris");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            HashMap<String,String> hashMap=new HashMap<>();//create a hashmap to store the data in key value pair
            hashMap.put("id_inventaris",String.valueOf(id));
            hashMap.put("kode_inventaris",kode);
            hashMap.put("nama",name);
            arrayList.add(hashMap);//add the hashmap into arrayList
        }
        String[] from={"kode_inventaris","nama"};//string array
        int[] to={R.id.RIKode,R.id.RINama};//int array of views id's
        simpleAdapter = new SimpleAdapter(this, arrayList, R.layout.rowinventaris, from, to);//Create object and set the parameters for simpleAdapter
        addbarang.setAdapter(simpleAdapter);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        HashMap row=(HashMap) simpleAdapter.getItem(position);
        JSONObject jo = null;
        try {
            jo = new JSONObject(row);
            id_inventaris = jo.getInt("id_inventaris");
            kode = jo.getString("kode_inventaris");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ETJumlahPinjam.setText("0");
        fillForm(id_inventaris);
    }

    void fillForm(int id_inventaris){
        APIAccess API = new APIAccess();
        JSONObject response = API.GET("api/getPeminjaman/"+String.valueOf(id_inventaris),access_token);
        try {
            if(!response.getJSONObject("data").isNull("terpinjam")){
                Jumlah = response.getJSONObject("data").getInt("jumlah")-Integer.parseInt(response.getJSONObject("data").getString("terpinjam"));
            }else{
                Jumlah = response.getJSONObject("data").getInt("jumlah");
            }
            Sisa = Jumlah;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ETJumlah.setText(String.valueOf(Jumlah));
        ETSisa.setText(String.valueOf(Sisa));
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        JumlahPinjam = 0;
        if(!ETJumlahPinjam.getText().toString().isEmpty()){
            JumlahPinjam = Integer.parseInt(ETJumlahPinjam.getText().toString());
        }
        Sisa = Jumlah - JumlahPinjam;
        ETSisa.setText(String.valueOf(Sisa));
        if(Sisa < 0 || JumlahPinjam == 0){
            Tambah.setVisibility(View.GONE);
        }else{
            Tambah.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        int c;

        for(c=0;c<ids.size();c++){
            if(ids.get(c)==id_inventaris){
                break;
            }
        }

        if(c==ids.size()){
            ids.add(id_inventaris);
            kodes.add(kode);
            jumlahs.add(JumlahPinjam);
        }else{
            jumlahs.set(c,JumlahPinjam);
        }
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Intent i = new Intent(this,MainActivity.class);
        Bundle data = new Bundle();
        data.putIntegerArrayList("ids",ids);
        data.putStringArrayList("kodes",kodes);
        data.putIntegerArrayList("jumlahs",jumlahs);
        data.putString("access_token",access_token);
        i.putExtras(data);
        startActivity(i);
    }
}
