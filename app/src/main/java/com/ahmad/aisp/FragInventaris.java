package com.ahmad.aisp;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragInventaris.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragInventaris#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragInventaris extends Fragment implements AdapterView.OnItemClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    TextView test;
    ListView inventarises;
    SimpleAdapter simpleAdapter;

    // TODO: Rename and change types of parameters
    private String access_token;

    private OnFragmentInteractionListener mListener;

    public FragInventaris() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment FragInventaris.
     */
    // TODO: Rename and change types and number of parameters
    public static FragInventaris newInstance(String param1) {
        FragInventaris fragment = new FragInventaris();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            access_token = getArguments().getString(ARG_PARAM1);
        }
    }
    @Override
    public void onViewCreated(View v, @Nullable Bundle saveInstanceState){
        inventarises = (ListView)v.findViewById(R.id.LVInventaris);
        this.fetchdata();
        inventarises.setOnItemClickListener(this);
    }

    private void fetchdata(){
        APIAccess API = new APIAccess();
        JSONObject response = API.GET("api/allInventaris",access_token);
        JSONArray data = null;
        try {
            data = response.getJSONArray("data");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        ArrayList<HashMap<String,String>> arrayList=new ArrayList<>();
        for(int i=0; i < data.length() ; i++) {
            JSONObject json_data = null;
            int id=0;
            String name = null,kode = null;
            try {
                json_data = data.getJSONObject(i);
                id=json_data.getInt("id_inventaris");
                name= json_data.getString("nama");
                kode= json_data.getString("kode_inventaris");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            HashMap<String,String> hashMap=new HashMap<>();//create a hashmap to store the data in key value pair
            hashMap.put("id_inventaris",String.valueOf(id));
            hashMap.put("kode_inventaris",kode);
            hashMap.put("nama",name);
            arrayList.add(hashMap);//add the hashmap into arrayList
        }
        String[] from={"kode_inventaris","nama"};//string array
        int[] to={R.id.RIKode,R.id.RINama};//int array of views id's
        simpleAdapter=new SimpleAdapter(getActivity(),arrayList,R.layout.rowinventaris,from,to);//Create object and set the parameters for simpleAdapter
        inventarises.setAdapter(simpleAdapter);//sets the adapter for listView
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.frag_inventaris, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        HashMap row=(HashMap) simpleAdapter.getItem(position);
        JSONObject jo = null;
        int gottenid=0;
        try {
            jo = new JSONObject(row);
            gottenid = jo.getInt("id_inventaris");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Intent i = new Intent(getActivity(),InvDetActivity.class);
        Bundle data = new Bundle();
        data.putString("id_inventaris", String.valueOf(gottenid));
        data.putString("access_token", access_token);
        getActivity().finish();
        i.putExtras(data);
        startActivity(i);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
