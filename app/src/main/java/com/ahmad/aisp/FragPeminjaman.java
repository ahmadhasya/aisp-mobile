package com.ahmad.aisp;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link FragPeminjaman.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragPeminjaman#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FragPeminjaman extends Fragment implements View.OnClickListener, AdapterView.OnItemLongClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static Bundle allPeminjamanParam;
    // TODO: Rename and change types of parameters
    private String access_token;
    private Bundle allPeminjaman;
    FloatingActionButton AddBarang;
    TextView Status,LabelPinjam,LabelKembali;
    ListView ListPeminjaman;
    TextInputEditText Nama,NIP,Alamat;
    DatePicker Kembali,Pinjam;
    Button Submit;

    private OnFragmentInteractionListener mListener;

    public FragPeminjaman() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment FragPeminjaman.
     */
    // TODO: Rename and change types and number of parameters
    public static FragPeminjaman newInstance(Bundle data) {
        FragPeminjaman fragment = new FragPeminjaman();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, data.getString("access_token"));
        args.putBundle(String.valueOf(allPeminjamanParam),data);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            access_token = getArguments().getString(ARG_PARAM1);
            allPeminjaman=getArguments().getBundle(String.valueOf(allPeminjamanParam));
        }
    }

    @Override
    public void onViewCreated(View v, @Nullable Bundle saveInstanceState){
        AddBarang = (FloatingActionButton) v.findViewById(R.id.AddBarang);
        ListPeminjaman = (ListView)v.findViewById(R.id.LVPeminjaman);
        Nama = (TextInputEditText)v.findViewById(R.id.NamaPegawai);
        NIP = (TextInputEditText)v.findViewById(R.id.NIPPegawai);
        Alamat = (TextInputEditText)v.findViewById(R.id.AlamatPegawai);
        Pinjam = (DatePicker) v.findViewById(R.id.TanggalPinjam);
        Kembali = (DatePicker) v.findViewById(R.id.TanggalKembali);
        Submit = (Button)v.findViewById(R.id.SubmitPeminjaman);
        Status = (TextView)v.findViewById(R.id.Status);
        LabelPinjam = (TextView)v.findViewById(R.id.LabelPinjam);
        LabelKembali = (TextView)v.findViewById(R.id.LabelKembali);

        cekAllPeminjaman();
        AddBarang.setOnClickListener(this);
        ListPeminjaman.setOnItemLongClickListener(this);
        Submit.setOnClickListener(this);
    }
    void cekAllPeminjaman(){
        if(allPeminjaman.getIntegerArrayList("ids")!=null){
            if(allPeminjaman.getIntegerArrayList("ids").size()==0){
                Status.setVisibility(View.VISIBLE);
                hidePeminjaman();
            }else{
                Status.setVisibility(View.GONE);
                showPeminjaman();
            }
        }else{
            Status.setVisibility(View.VISIBLE);
            hidePeminjaman();
        }
    }
    void hidePeminjaman(){
        ListPeminjaman.setVisibility(View.GONE);
        Nama.setVisibility(View.GONE);
        NIP.setVisibility(View.GONE);
        Alamat.setVisibility(View.GONE);
        Pinjam.setVisibility(View.GONE);
        Kembali.setVisibility(View.GONE);
        Submit.setVisibility(View.GONE);
        LabelKembali.setVisibility(View.GONE);
        LabelPinjam.setVisibility(View.GONE);
    }

    void showPeminjaman(){
        ArrayList<HashMap<String,String>> arrayList=new ArrayList<>();
        for(int i=0; i < allPeminjaman.getIntegerArrayList("ids").size() ; i++) {

            int id=0,jumlah = 0;
            String kode = null;
            id = allPeminjaman.getIntegerArrayList("ids").get(i);
            kode = allPeminjaman.getStringArrayList("kodes").get(i);
            jumlah = allPeminjaman.getIntegerArrayList("jumlahs").get(i);
            HashMap<String,String> hashMap=new HashMap<>();//create a hashmap to store the data in key value pair
            hashMap.put("id_inventaris",String.valueOf(id));
            hashMap.put("kode_inventaris",kode);
            hashMap.put("jumlah", String.valueOf(jumlah));
            arrayList.add(hashMap);//add the hashmap into arrayList
        }
        String[] from={"kode_inventaris","jumlah"};//string array
        int[] to={R.id.RIKode,R.id.RINama};//int array of views id's
        SimpleAdapter simpleAdapter = new SimpleAdapter(getActivity(), arrayList, R.layout.rowinventaris, from, to);//Create object and set the parameters for simpleAdapter
        ListPeminjaman.setAdapter(simpleAdapter);//sets the adapter for listView
        ListPeminjaman.setVisibility(View.VISIBLE);
        Nama.setVisibility(View.VISIBLE);
        NIP.setVisibility(View.VISIBLE);
        Alamat.setVisibility(View.VISIBLE);
        Pinjam.setVisibility(View.VISIBLE);
        Kembali.setVisibility(View.VISIBLE);
        Submit.setVisibility(View.VISIBLE);
        LabelKembali.setVisibility(View.VISIBLE);
        LabelPinjam.setVisibility(View.VISIBLE);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.frag_peminjaman, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()==R.id.AddBarang){
            Intent i = new Intent(getActivity(),AddPeminjamanActivity.class);
            i.putExtras(allPeminjaman);
            getActivity().finish();
            startActivity(i);
        }else if(v.getId()==R.id.SubmitPeminjaman){
            int year = Pinjam.getYear(), monthOfYear = Pinjam.getMonth()+1,dayOfMonth = Pinjam.getDayOfMonth();

            String tanggalPinjam = year+"-"+(monthOfYear<10?"0"+monthOfYear:monthOfYear)+"-"+(dayOfMonth<10?"0"+dayOfMonth:dayOfMonth);

            SimpleDateFormat formatTanggal = new SimpleDateFormat("yyyy-mm-dd");
            Date nilaiPinjam = null,nilaiKembali=null;

            year = Kembali.getYear();
            monthOfYear = Kembali.getMonth()+1;
            dayOfMonth = Kembali.getDayOfMonth();
            String tanggalKembali = year+"-"+(monthOfYear<10?"0"+monthOfYear:monthOfYear)+"-"+(dayOfMonth<10?"0"+dayOfMonth:dayOfMonth);

            try {
                nilaiPinjam = formatTanggal.parse(tanggalPinjam);
                nilaiKembali = formatTanggal.parse(tanggalKembali);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            if(isValid(nilaiKembali,nilaiPinjam)){
                submit(tanggalKembali,tanggalPinjam);
                hidePeminjaman();
            }else{
                Toast.makeText(getActivity().getApplicationContext(),"Data Yang dimasukkan tidak valid.",Toast.LENGTH_SHORT).show();
            }
        }
    }

    boolean isValid(Date nilaiKembali,Date nilaiPinjam){
        return (nilaiKembali.compareTo(nilaiPinjam)>=0)&&(Nama.getText().length()>0)&&(NIP.getText().length()>0)&&(Alamat.getText().length()>0);
    }

    void submit(String tanggalKembali, String tanggalPinjam){
        APIAccess API = new APIAccess();
        String inventarises = "[";
        for(int i=0;i<allPeminjaman.getIntegerArrayList("ids").size();i++){
            inventarises = inventarises +
                    "{" +
                    "'Inventaris':{'id_inventaris':'"+allPeminjaman.getIntegerArrayList("ids").get(i)+"'}," +
                    "'JumlahPinjam':'"+allPeminjaman.getIntegerArrayList("jumlahs").get(i)+"'" +
                    "}"+(i==allPeminjaman.getIntegerArrayList("ids").size()-1?"":",");
        }
        inventarises = inventarises + "]";

        String json = "{'Inventaris':"+inventarises+"," +
                "'NamaPegawai':'"+Nama.getText().toString()+"'," +
                "'NIPPegawai':'"+NIP.getText().toString()+"'," +
                "'AlamatPegawai':'"+Alamat.getText().toString()+"'," +
                "'TanggalPinjam':'"+tanggalPinjam+"'," +
                "'TanggalKembali':"+tanggalKembali+"" +
                "}";
        JSONObject response = API.POST("/api/addPeminjaman",access_token,json);
        try {
            if(response.getJSONObject("data")!=null){
                Toast.makeText(getActivity().getApplicationContext(),"Data telah ditambahkan.",Toast.LENGTH_SHORT).show();
                Intent i = new Intent(getActivity(),MainActivity.class);

                Bundle data = new Bundle();
                data.putString("access_token",access_token);
                getActivity().finish();
                i.putExtras(data);
                startActivity(i);
            }else{
                Toast.makeText(getActivity().getApplicationContext(),response.getString("message"),Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        allPeminjaman.getIntegerArrayList("ids").remove(position);
        allPeminjaman.getStringArrayList("kodes").remove(position);
        allPeminjaman.getIntegerArrayList("jumlahs").remove(position);
        cekAllPeminjaman();
        return false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
