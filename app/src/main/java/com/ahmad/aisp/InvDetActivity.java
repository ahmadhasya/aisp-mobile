package com.ahmad.aisp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

public class InvDetActivity extends AppCompatActivity implements View.OnClickListener {
    String id_inventaris,access_token,nama,kondisi,keterangan,
            jumlah,tanggal_register,kode_inventaris,terpinjam,kode_ruang,kode_jenis;

    TextInputEditText DetailNama,DetailKondisi,DetailKeterangan,DetailJumlah,
            DetailTanggalRegister,DetailKode,DetailTerpinjam,DetailRuang,DetailJenis;
    Button Delete,Save;
    JSONObject response;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inv_det);
        Bundle data = getIntent().getExtras();
        id_inventaris = data.getString("id_inventaris");
        access_token = data.getString("access_token");
        DetailNama = (TextInputEditText) findViewById(R.id.nama_detail_inventaris);
        DetailKondisi = (TextInputEditText) findViewById(R.id.kondisi_detail_inventaris);
        DetailKeterangan = (TextInputEditText) findViewById(R.id.keterangan_detail_inventaris);
        DetailJumlah = (TextInputEditText) findViewById(R.id.jumlah_detail_inventaris);
        DetailTanggalRegister = (TextInputEditText) findViewById(R.id.tanggal_register_detail_inventaris);
        DetailKode = (TextInputEditText) findViewById(R.id.kode_detail_inventaris);
        DetailTerpinjam = (TextInputEditText) findViewById(R.id.terpinjam_detail_inventaris);
        DetailRuang = (TextInputEditText) findViewById(R.id.kode_ruang_detail_inventaris);
        DetailJenis = (TextInputEditText) findViewById(R.id.kode_jenis_detail_inventaris);
        Delete = (Button)findViewById(R.id.InvDelete);
        Save = (Button)findViewById(R.id.InvSave);

        Save.setOnClickListener(this);
        Delete.setOnClickListener(this);

        APIAccess API = new APIAccess();
        response = API.GET("api/getInventaris/"+id_inventaris,access_token);
        try {
            nama = response.getJSONObject("data").getString("nama");
            kondisi = response.getJSONObject("data").getString("kondisi");
            keterangan = response.getJSONObject("data").getString("keterangan");
            jumlah = response.getJSONObject("data").getString("jumlah");
            tanggal_register = response.getJSONObject("data").getString("tanggal_register");
            kode_inventaris = response.getJSONObject("data").getString("kode_inventaris");
            terpinjam = response.getJSONObject("data").getString("terpinjam");
            kode_ruang = response.getJSONObject("data").getJSONObject("ruang").getString("kode_ruang");
            kode_jenis = response.getJSONObject("data").getJSONObject("jenis").getString("kode_jenis");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        DetailNama.setText(nama);
        DetailKondisi.setText(kondisi);
        DetailKeterangan.setText(keterangan);
        DetailJumlah.setText(jumlah);
        DetailTanggalRegister.setText(tanggal_register);
        DetailKode.setText(kode_inventaris);
        DetailTerpinjam.setText(terpinjam);
        DetailRuang.setText(kode_ruang);
        DetailJenis.setText(kode_jenis);
    }

    @Override
    public void onClick(View v) {
        APIAccess API = new APIAccess();
        if(v.getId()==R.id.InvSave){
            Save();
            Toast.makeText(getApplicationContext(),"Data Inventaris Tersimpan",Toast.LENGTH_SHORT).show();
        }else if(v.getId()==R.id.InvDelete){
            JSONObject response = API.GET("api/deleteInventaris/"+id_inventaris,access_token);
            Toast.makeText(getApplicationContext(),"Data Inventaris Terhapus",Toast.LENGTH_SHORT).show();
        }
        finish();
    }

    private void Save() {
        String jsonPOST = null;
        try {
            jsonPOST = "{" +
                    "'idInventaris':"+id_inventaris+"," +
                    "'Jenis':"+response.getJSONObject("data").getJSONObject("jenis").getInt("id_jenis")+"," +
                    "'Ruang':"+response.getJSONObject("data").getJSONObject("ruang").getInt("id_ruang")+"," +
                    "'Jumlah':'"+DetailJumlah.getText().toString()+"'," +
                    "'keteranganInventarisir':'"+DetailKeterangan.getText().toString()+"'," +
                    "'Kondisi':'"+DetailKondisi.getText().toString()+"'," +
                    "'kodeInventarisir':'"+DetailKode.getText().toString()+"'," +
                    "'namaInventarisir':'"+DetailNama.getText().toString()+"'," +
                    "'keteranganInventarisir':'"+DetailKeterangan.getText().toString()+"'" +
                    "}";
        } catch (JSONException e) {
            e.printStackTrace();
        }
        APIAccess API = new APIAccess();
        JSONObject ObjectResponse = API.POST("api/addInventaris",access_token,jsonPOST);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Intent i = new Intent(getApplicationContext(), MainActivity.class);
        Bundle data = new Bundle();
        data.putString("access_token", access_token);
        i.putExtras(data);
        startActivity(i);
    }
}
