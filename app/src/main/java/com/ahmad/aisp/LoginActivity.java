package com.ahmad.aisp;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;



public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    TextInputEditText username,password;
    Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        if (android.os.Build.VERSION.SDK_INT > 9)
        {
            StrictMode.ThreadPolicy policy = new
                    StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        username = (TextInputEditText) findViewById(R.id.username);
        password = (TextInputEditText) findViewById(R.id.password);
        login    = (Button)findViewById(R.id.login);

        login.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        String jsonPOST = "{" +
                "'grant_type':'password'," +
                "'client_id':'3'," +
                "'client_secret':'bhV3ft95V2lTW46chAThx3KCyodvHnUzujaXxF9S'," +
                "'username':'"+username.getText().toString()+"'," +
                "'password':'"+password.getText().toString()+"'}";
        APIAccess API = new APIAccess();
        JSONObject ObjectResponse = API.POST("oauth/token","no",jsonPOST);

        String response="";

        if(ObjectResponse.isNull("access_token")){
            try {
                response = ObjectResponse.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Toast.makeText(getApplicationContext(),response,Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(getApplicationContext(),"Selamat datang !",Toast.LENGTH_LONG).show();
            Intent i = new Intent(this,MainActivity.class);
            Bundle data = new Bundle();
            try {
                response = ObjectResponse.getString("access_token");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            data.putString("access_token",response);
            i.putExtras(data);
            finish();
            startActivity(i);
        }
    }
}
