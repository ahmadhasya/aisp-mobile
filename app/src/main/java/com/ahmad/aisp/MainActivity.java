package com.ahmad.aisp;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements
        FragInventaris.OnFragmentInteractionListener,
        FragPeminjaman.OnFragmentInteractionListener,
        FragPengembalian.OnFragmentInteractionListener,
        FragPengaturan.OnFragmentInteractionListener,
        BottomNavigationView.OnNavigationItemSelectedListener {

    public String nama_petugas, access_token;
    public int id_level=0;
    ArrayList<String> kodes;
    ArrayList<Integer> ids,jumlahs;

    Bundle data;

    void showInventaris(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame, FragInventaris.newInstance(access_token))
                .commit();
    }
    void showPeminjaman(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame, FragPeminjaman.newInstance(data))
                .commit();
    }
    void showPengembalian(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame, FragPengembalian.newInstance(access_token))
                .commit();
    }
    void showPengaturan(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame, FragPengaturan.newInstance(access_token,nama_petugas))
                .commit();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        data = getIntent().getExtras();
        access_token = data.getString("access_token");
        jumlahs = data.getIntegerArrayList("jumlahs");
        ids = data.getIntegerArrayList("ids");
        kodes = data.getStringArrayList("kodes");
        getUser();
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.getMenu().removeItem(R.id.navigation_inventaris);
        navigation.getMenu().removeItem(R.id.navigation_pengembalian);
        showPeminjaman();
//        if(id_level!=1){
//            navigation.getMenu().removeItem(R.id.navigation_inventaris);
//        }else{
//            showInventaris();
//        }
//        if(id_level==3){
//            navigation.getMenu().removeItem(R.id.navigation_pengembalian);
//        }
        navigation.setOnNavigationItemSelectedListener(this);
    }

    public void getUser(){
        APIAccess API = new APIAccess();
        JSONObject ObjectResponse = API.GET("api/user",access_token);
        try {
            id_level = Integer.parseInt(ObjectResponse.getString("id_level"));
            nama_petugas = ObjectResponse.getString("nama_petugas");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.navigation_inventaris:
                showInventaris();
                return true;
            case R.id.navigation_peminjaman:
                showPeminjaman();
                return true;
            case R.id.navigation_pengembalian:
                showPengembalian();
                return true;
            case R.id.navigation_pengaturan:
                showPengaturan();
                return true;
        }
        return false;
    }
    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
